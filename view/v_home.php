<html>
<head>
    <link href="styles/style.css" rel="stylesheet">
    <title>Film</title>
</head>

<body>
     <h1 id="header">Liste des Films<h1>
     <ul class ="menu">
     <li>
        <a href="#">Accueil</a>
    </li>
   
    <li>
        <a href="#">Films</a>
    </li>
    <li>
        <a href="controlleur/c_inscription.php">Inscription</a>
    </li>
    <li>
         <a href="view/v_connexion.php">Connexion</a>
     </li>
</ul>
     <table border="1">
          <thead>
               <?php foreach ($listColumn as $key =>$row) { ?>
                    <th><?php echo $row; ?>
                    <a href="index.php?orderBy=<?php echo $row?>&orderDirection=ASC" class="up">⬆️</a>
                    <a href="index.php?orderBy=<?php echo $row ; ?>&orderDirection=DESC"class="down">⬇️</a>
               </th>
               <?php } ?>
          </thead>
          <tbody>

               <?php 
                
                foreach ($listFilm as $key=>$row){
               ?>
               <tr>
                    <td> <?php echo $row["id"]; ?></td>
                    <td> <?php echo $row["nom"]; ?></td>
                    <td> <?php echo $row["annee"]; ?> </td>
                    <td> <?php echo $row["score"]; ?> </td>
                    <td> <?php echo $row["nbVotants"]; ?> </td>
               </tr>
               <?php } ?>
          </tbody>
</table>
</body>
</html>             